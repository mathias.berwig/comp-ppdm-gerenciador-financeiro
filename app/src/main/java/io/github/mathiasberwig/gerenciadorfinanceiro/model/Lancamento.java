package io.github.mathiasberwig.gerenciadorfinanceiro.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.Exclude;

import java.util.Date;

import io.github.mathiasberwig.gerenciadorfinanceiro.Formatos;

/**
 * Representa um lançamento financeiro, com dia, valor e descrição.
 */
public class Lancamento implements Parcelable {

    private Date   dia;
    private long   valor;
    private String descricao;

    public Lancamento() {
    }

    public Lancamento(Date dia, long valor, String descricao) {
        this.dia = dia;
        this.valor = valor;
        this.descricao = descricao;
    }

    public Date getDia() {
        return dia;
    }

    @Exclude
    public String getDataFormatada() {
        return Formatos.DATE_FORMATTER.format(dia.toInstant());
    }

    public long getValor() {
        return valor;
    }

    @Exclude
    public String getValorFormatado() {
        return Formatos.CURRENCY_FORMATTER.format((double) valor / 100);
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return "Lancamento{" +
                ", dia=" + dia +
                ", valor=" + valor +
                ", descricao='" + descricao + '\'' +
                '}';
    }

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.dia != null ? this.dia.getTime() : -1);
        dest.writeLong(this.valor);
        dest.writeString(this.descricao);
    }

    protected Lancamento(Parcel in) {
        long tmpData = in.readLong();
        this.dia = tmpData == -1 ? null : new Date(tmpData);
        this.valor = in.readLong();
        this.descricao = in.readString();
    }

    public static final Parcelable.Creator<Lancamento> CREATOR = new Parcelable.Creator<Lancamento>() {
        @Override
        public Lancamento createFromParcel(Parcel source) {
            return new Lancamento(source);
        }

        @Override
        public Lancamento[] newArray(int size) {
            return new Lancamento[size];
        }
    };
    //endregion
}
