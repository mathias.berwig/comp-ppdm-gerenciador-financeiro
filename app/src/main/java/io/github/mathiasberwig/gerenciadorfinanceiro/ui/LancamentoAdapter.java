package io.github.mathiasberwig.gerenciadorfinanceiro.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.mathiasberwig.gerenciadorfinanceiro.R;
import io.github.mathiasberwig.gerenciadorfinanceiro.model.Lancamento;

/**
 * Adaptador responsável por exibir uma lista de {@link Lancamento Lancamentos} a partir de uma
 * consulta do Firebase Firestore.
 */
public class LancamentoAdapter extends FirestoreRecyclerAdapter<Lancamento, LancamentoAdapter.ViewHolder> {

    private final LancamentoClickListener listener;

    public LancamentoAdapter(@NonNull FirestoreRecyclerOptions<Lancamento> options,
                             LancamentoClickListener listener) {
        super(options);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.lanc_list_item, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder,
                                    int pos,
                                    @NonNull Lancamento lancamento) {
        holder.bind(lancamento);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final LancamentoClickListener listener;
        private Lancamento lancamento;

        ViewHolder(@NonNull View itemView, LancamentoClickListener listener) {
            super(itemView);
            this.itemView.setOnClickListener(this);
            this.listener = listener;
            ButterKnife.bind(this, itemView);
        }

        @BindView(R.id.lanc_data)  TextView data;
        @BindView(R.id.lanc_valor) TextView valor;
        @BindView(R.id.lanc_descr) TextView descricao;

        void bind(Lancamento l) {
            this.lancamento = l;

            data.setText(l.getDataFormatada());
            valor.setText(l.getValorFormatado());
            valor.setSelected(l.getValor() >= 0);
            descricao.setText(l.getDescricao());
        }

        @Override
        public void onClick(View v) {
            if (listener != null) listener.onLancamentoClick(lancamento, getAdapterPosition());
        }
    }

    public interface LancamentoClickListener {
        void onLancamentoClick(Lancamento lancamento, int position);
    }
}
