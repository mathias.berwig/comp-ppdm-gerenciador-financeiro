package io.github.mathiasberwig.gerenciadorfinanceiro;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Formatos {

    public static final Locale locale = Locale.forLanguageTag("pt-BR");
    static {
        Locale.setDefault(locale);
    }

    public static final NumberFormat CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance(locale);

    public static final DateTimeFormatter DATE_FORMATTER =
            DateTimeFormatter
                .ofLocalizedDate(FormatStyle.SHORT)
                .withLocale(locale)
                .withZone(ZoneId.systemDefault());
}
