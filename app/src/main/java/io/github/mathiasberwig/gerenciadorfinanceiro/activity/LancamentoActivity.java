package io.github.mathiasberwig.gerenciadorfinanceiro.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.mathiasberwig.gerenciadorfinanceiro.Formatos;
import io.github.mathiasberwig.gerenciadorfinanceiro.R;
import io.github.mathiasberwig.gerenciadorfinanceiro.firebase.FirestoreContract;
import io.github.mathiasberwig.gerenciadorfinanceiro.model.Lancamento;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

/**
 * Activity que permite ao usuário criar, editar e excluir lançamentos.
 */
public class LancamentoActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "LancamentoActivity";

    private static final String EXTRA_LANCAMENTO = TAG + ".LANCAMENTO";
    private static final String EXTRA_REFERENCIA = TAG + ".REFERENCIA";

    public static void start(Context context) {
        context.startActivity(new Intent(context, LancamentoActivity.class));
    }

    public static void start(Context context, Lancamento lancamento, String ref) {
        final Bundle extras = new Bundle(1);
        extras.putParcelable(EXTRA_LANCAMENTO, lancamento);
        extras.putString(EXTRA_REFERENCIA, ref);

        final Intent intent = new Intent(context, LancamentoActivity.class);
        intent.putExtras(extras);

        context.startActivity(intent);
    }

    private Lancamento lancamento;
    private String referencia;

    @BindView(R.id.lanc_input_valor)      CurrencyEditText  inputValor;
    @BindView(R.id.lanc_input_data)       TextInputEditText inputData;
    @BindView(R.id.lanc_input_descricao)  TextInputEditText inputDescricao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lanc_activity);

        ButterKnife.bind(this);

        final ActionBar toolbar = Objects.requireNonNull(getSupportActionBar());
        toolbar.setDisplayHomeAsUpEnabled(true);

        inputValor.setLocale(Formatos.locale);

        if (getIntent() != null && getIntent().getExtras() != null) {
            final Bundle extras = getIntent().getExtras();
            lancamento = extras.getParcelable(EXTRA_LANCAMENTO);
            referencia = extras.getString(EXTRA_REFERENCIA);
            writeToUi();
            disableFieldsOnEdit();
        }

        setTitle(referencia == null ? R.string.title_add_lancamento : R.string.title_edit_lancamento);

        updateData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (lancamento != null && referencia != null) {
            getMenuInflater().inflate(R.menu.lanc_remove, menu);
        }
        getMenuInflater().inflate(R.menu.lanc_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Home / Voltar
            case android.R.id.home: {
                finish();
                return true;
            }
            // Remover
            case R.id.lanc_remove: {

                FirebaseFirestore.getInstance()
                        .collection(FirestoreContract.LANCAMENTOS)
                        .document(referencia)
                        .delete();

                finish();
                return true;
            }
            // Salvar
            case R.id.lanc_save: {
                final Lancamento lancamento = readFromUi();

                final CollectionReference lancamentos = FirebaseFirestore.getInstance()
                        .collection(FirestoreContract.LANCAMENTOS);

                if (referencia == null) {
                    // Novo lançamento
                    lancamentos.add(lancamento);
                } else {
                    // Atualizar lançamento existente
                    lancamentos.document(referencia).set(lancamento);
                }

                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * @return lê os dados da interface do usuário e retorna-os em formato de Lançamento.
     */
    private Lancamento readFromUi() {
        final long valor = inputValor.getRawValue();
        final Date data  = cal.getTime();
        final String descricao = Objects.requireNonNull(inputDescricao.getText()).toString();

        return new Lancamento(data, valor, descricao);
    }

    /**
     * Grava os dados do {@link #lancamento} na interface do usuário.
     */
    private void writeToUi() {
        inputValor.setValue(lancamento.getValor());
        inputData.setText(lancamento.getDataFormatada());
        cal.setTime(lancamento.getDia());
        inputDescricao.setText(lancamento.getDescricao());
    }

    //region Input Data
    private Calendar cal = Calendar.getInstance();

    @OnClick(R.id.lanc_input_data)
    void onDataClick() {
        new DatePickerDialog(
                LancamentoActivity.this,
                this,
                cal.get(YEAR),
                cal.get(MONTH),
                cal.get(DAY_OF_MONTH)
        ).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        updateData();
    }

    /**
     * Atualiza o TextView de data com o valor obtido do DatePicker.
     */
    private void updateData() {
        final String date = Formatos.DATE_FORMATTER.format(cal.toInstant());
        inputData.setText(date);
    }

    /**
     * Desabilita os campos de Valor e Data quando o app estiver em modo de edição (não é permitido
     * alterar esses campos após a criação).
     */
    private void disableFieldsOnEdit() {
        inputValor.setEnabled(false);
        inputData.setEnabled(false);
    }
    //endregion
}