package io.github.mathiasberwig.gerenciadorfinanceiro.activity;

import android.os.Bundle;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.mathiasberwig.gerenciadorfinanceiro.R;
import io.github.mathiasberwig.gerenciadorfinanceiro.firebase.FirestoreContract;
import io.github.mathiasberwig.gerenciadorfinanceiro.model.Lancamento;
import io.github.mathiasberwig.gerenciadorfinanceiro.model.Saldo;
import io.github.mathiasberwig.gerenciadorfinanceiro.ui.LancamentoAdapter;

/**
 * Tela inicial com o gráfico de histórico e lista de lançamentos.
 */
public class InicioActivity extends AppCompatActivity implements LancamentoAdapter.LancamentoClickListener {

    //region Android Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio_activity);

        ButterKnife.bind(this);

        setupLista();
        setupGrafico();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Começa a receber atualizações na lista de lançamentos
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Para de receber atualizações na lista de lançamentos
        adapter.stopListening();
    }
    //endregion

    //region FAB
    @OnClick(R.id.fab)
    void onFabClick() {
        LancamentoActivity.start(this);
    }
    //endregion

    //region Database
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    //endregion

    //region RecyclerView
    @BindView(R.id.lanc_lista) RecyclerView lista;

    private LancamentoAdapter adapter;

    private void setupLista() {
        // Carrega a lista de lançamentos ordenada de forma descendente (mais recente primeiro)
        // com no máximo 50 registros
        final Query listBaseQuery = db.collection(FirestoreContract.LANCAMENTOS)
                .orderBy("dia", Query.Direction.DESCENDING)
                .limit(50);

        // Cria as configurações para o adaptador da lista
        FirestoreRecyclerOptions<Lancamento> options = new FirestoreRecyclerOptions.Builder<Lancamento>()
                .setLifecycleOwner(this)
                .setQuery(listBaseQuery, Lancamento.class)
                .build();

        adapter = new LancamentoAdapter(options, this);

        lista.setAdapter(adapter);
    }

    @Override
    public void onLancamentoClick(Lancamento lancamento, int pos) {
        // Obtém o ID do lançamento selecionado
        final String id = adapter.getSnapshots().getSnapshot(pos).getReference().getId();

        LancamentoActivity.start(this, lancamento, id);
    }
    //endregion

    @BindView(R.id.chart) BarChart chart;

    private final Query chartBaseQuery = db.collection(FirestoreContract.SALDO).limit(30);

    private ArrayList<Saldo> saldos;

    private void setupGrafico() {
        // Remove a descrição
        chart.getDescription().setEnabled(false);

        // Desabilita gestos de toque
        chart.setTouchEnabled(false);

        // Habilita gestos de zoom
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // Desabilita gesto de pinch-zoom
        chart.setPinchZoom(false);

        // Desabilita o desenho de background
        chart.setDrawGridBackground(false);
        chart.setMaxHighlightDistance(300);

        // Configura o Eixo X
        XAxis x = chart.getXAxis();
        x.setEnabled(true);
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setValueFormatter((value, axis) -> {
            // Formata apenas os primeiros itens de cada grupo (de índice par)
            if (saldos == null || (int) value % 2 == 1) {
                return "";
            }

            // Calcula a posição do Saldo que gerou aquela linha do gráfico, já que são geradas
            // duas linhas para cada
            final int i = (int) value / 2;

            return saldos.get(i).id;
        });

        // Configura o Eixo Y (desabilita-o)
        YAxis y = chart.getAxisLeft();
        y.setEnabled(false);

        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);

        // Executa a query de consulta do gráfico de modo assíncrono
        chartBaseQuery.get()
                .addOnSuccessListener(
                        this,
                        documentSnaps -> {
                            // Converte os Snapshots em um ArrayList<Saldo>
                            saldos = saldoSnapsToArray(documentSnaps.getDocuments());

                            final ArrayList<BarEntry> receitas = new ArrayList<>();
                            final ArrayList<BarEntry> despesas = new ArrayList<>();

                            // Cria uma linha para cada item de despesa e receita do dia
                            // Para todos os dias
                            int j = 0;
                            for (int i = 0; i < saldos.size(); i++) {
                                Saldo saldo = saldos.get(i);
                                despesas.add(new BarEntry(j++, saldo.despesas));
                                receitas.add(new BarEntry(j++, saldo.receitas));
                            }

                            setData(receitas, despesas);
                        }
                );
    }

    private void setData(ArrayList<BarEntry> receitas, ArrayList<BarEntry> despesas) {

        BarDataSet dsReceitas, dsDespesas;

        if (chart.getData() != null && chart.getData().getDataSetCount() == 2) {
            // Datasets já existem, apenas atualiza dados
            dsReceitas = (BarDataSet) chart.getData().getDataSetByIndex(0);
            dsReceitas.setValues(receitas);

            dsDespesas = (BarDataSet) chart.getData().getDataSetByIndex(1);
            dsDespesas.setValues(despesas);

            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // Cria e configura novos datasets
            dsReceitas = new BarDataSet(receitas, "Receitas");
            dsReceitas.setColor(getColor(R.color.lanc_positivo));

            dsDespesas = new BarDataSet(despesas, "Despesas");
            dsDespesas.setColor(getColor(R.color.lanc_negativo));

            BarData data = new BarData(dsReceitas, dsDespesas);
            data.setValueFormatter(new LargeValueFormatter());

            chart.setData(data);
        }

        chart.invalidate();
    }

    private ArrayList<Saldo> saldoSnapsToArray(List<DocumentSnapshot> documents) {

        final ArrayList<Saldo> saldos = new ArrayList<>(documents.size());

        Objects.requireNonNull(documents).forEach(
                documentSnapshot -> saldos.add(documentSnapshot.toObject(Saldo.class))
        );

        return saldos;
    }
}
