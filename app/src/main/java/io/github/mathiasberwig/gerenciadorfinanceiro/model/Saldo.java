package io.github.mathiasberwig.gerenciadorfinanceiro.model;

/**
 * Representa um dia com saldo de receitas e despesas.
 */
public class Saldo {

    /**
     * Identificador do item. Ou data. Depende de como você prefere chamar. Se vê-lo como uma data,
     * considere o formato yyyy-dd-mm.
     */
    public String id;

    /**
     * Soma das receitas de um dia.
     */
    public float receitas;

    /**
     * Soma das despesas de um dia.
     */
    public float despesas;

    public Saldo() {
    }
}
