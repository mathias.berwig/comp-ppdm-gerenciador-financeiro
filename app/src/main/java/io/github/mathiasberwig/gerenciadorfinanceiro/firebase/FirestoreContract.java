package io.github.mathiasberwig.gerenciadorfinanceiro.firebase;

/**
 * Define constantes para manipulação da persistência de dados.
 */
public class FirestoreContract {

    public static final String LANCAMENTOS = "lancamentos";
    public static final String SALDO = "saldo";
}
