const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

// Atualiza o saldo sempre que um doc é criado
exports.atualizaSaldoOnCreate = functions.firestore
    .document('lancamentos/{lancId}')
    .onCreate(lancamento => {
        // Obtem o valor e a data do lançamento
        var lancVal = getLancVal(lancamento);
        var lancData = getLancData(lancamento);

        // Não é necessária ação
        if (lancVal == 0) return;

        // Obtém a referência para o dia do saldo
        var saldoRef = getSaldoRef(lancData);

        // Carrega e atualiza o saldo
        return saldoRef.get()
            .then(saldo => {
                var key = lancVal > 0 ? 'receitas' : 'despesas';
                var val = lancVal > 0 ? lancVal : lancVal * -1;
                var oldVal = saldo.get(key);

                return saldoRef.set({
                    id: lancData,
                    [key]: oldVal === undefined ? val : oldVal + val
                }, { merge: true });
            });
    });

// Atualiza o saldo sempre que um doc é removido
exports.atualizaSaldoOnDelete = functions.firestore
    .document('lancamentos/{lancId}')
    .onDelete(lancamento => {
        // Obtem o valor e a data do lançamento
        var lancVal = getLancVal(lancamento);
        var lancData = getLancData(lancamento);

        // Não é necessária ação
        if (lancVal == 0) return;

        // Obtém a referência para o dia do saldo
        var saldoRef = getSaldoRef(lancData);

        // Carrega e atualiza o saldo
        return saldoRef.get()
            .then(saldo => {
                var key = lancVal > 0 ? 'receitas' : 'despesas';
                var val = lancVal > 0 ? lancVal * -1 : lancVal;
                var oldVal = saldo.get(key);

                return saldoRef.set({
                    [key]: oldVal + val
                }, { merge: true });
            });
    });


function getLancVal(lancamento) {
    return lancamento.get('valor') / 100;
}

function getLancData(lancamento) {
    return lancamento.get('dia').toDate().toISOString().replace(/T.+/, '');
}

function getSaldoRef(lancData) {
    return admin.firestore().collection('saldo').doc(lancData);
}